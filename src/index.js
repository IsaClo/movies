import 'bootstrap';
import './style.scss';

// l'utilisateur peut faire une recherche sur un titre par exemple
// on récupère la valeur de la barre de submit
// on va  chercher  dans la librairie de movies
// à chaque fois qu'un titre comporte la valeur de Search
// on ajout à notre page un élément de liste avec
// titre / image / roles principaux
$( function() {
    let $searchForm = $('#searchForm');
    let $inputSearch = $('#inputSearch', $searchForm);
    let $ulList = $('#ulList');
    let $detList = $('#detList');

    $searchForm.on( 'submit', function( event ) {
        event.preventDefault();
        let choice = $inputSearch.val();

        $.get( `https://api.themoviedb.org/3/search/movie?api_key=4c991eb23e1a25fd8b93f5dc9b577440&query=${choice}`, function( response ) {
            $ulList.html(' ');
            $detList.html( ' ');

            // Pour chaque données de notre tableau de response (ici response est un tableau d'objet en json, décodé par jQuery)
            for ( const singleData of response.results ) {

                // On initialise nos variables
                let title,
                    tpl,
                    tpl1,
                    vote,
                    id,
                    poster,
                    overview,
                    addr,
                    releaseDate;
                        // On récupère les données et on fait les templates  avec
                        title = singleData.title ;
                        vote = singleData.vote_average;
                        id = singleData.id;
                        poster = singleData.poster_path;
                        addr = `https://image.tmdb.org/t/p/w500${poster}`;
                        overview = singleData.overview;
                        releaseDate = singleData.release_date;

                        tpl = `
                                <a href="#detList">${title}</a>
                            ` ;
                        tpl1 = `
                                <h2>${title}</h2>
                                <ul><li>la moyenne des votes est de : ${vote}</li>
                                    <li> il a été réalisé en : ${releaseDate}</li>
                                    <li>${overview}</li>
                                    <li>${poster}</li></ul>
                                    <img src="${addr}" alt=""/>
                        `;
                // On crée l'élément li
                let $li = $( '<li />', {
                    html: tpl
                } );

                // On l'ajoute à notre liste
                $ulList.append( $li );

                // on créer l'élément détail
            let $det = $('<p />', {
                html: tpl1
            });
            // on ajoute au paragraphe details
                $detList.append( $det );
            }
        });
    });
});